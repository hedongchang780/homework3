var cart = {};
cart.array = [];

$(document).ready(function () {
    $('button.setting').click(button_clicked);
    $('button.seeing').click(see_my_cookie);
});

function button_clicked() {
    var $this = $(this);
    var value = $this.data('pretty-value');
    cart.array.push(value);
    var cookie_string = JSON.stringify(cart);
    document.cookie = 'cookiekey=' + cookie_string + '; max-age=600; path=/; domain=';
    //document.cookie = 'cookiekey=thecookievalue; max-age=600; path=/; domain=';
}

function see_my_cookie() {
    var cookie_array = document.cookie.split('; ');
    var cookiekey = 'cookiekey=';
    var cookieval;
    for (var i = 0; i < cookie_array.length; i++) {
        if (cookie_array[i].substring(0, cookiekey.length) == cookiekey) {
            cookieval = cookie_array[i].substring(cookiekey.length);
        }
    }
    //var cookiekey = 'cookiekey';
    console.log(cookieval)
    cart = JSON.parse(cookieval);
    console.log(cart);
}