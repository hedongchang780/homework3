var mapDemo = {};
   var data =  [];

$(document).ready(function(){
        populate();
        
})

function clear_map() {
        for (var i = 0; i < mapDemo.markers.length; i++) {
        mapDemo.markers[i].setMap(null);
        }
}

function ajax_success(response) {
    data = JSON.parse(response); 
     mapDemo.mapOptions = {
                center: new google.maps.LatLng(47.655, -122.308),
                zoom: 15
        }

        mapDemo.map = new google.maps.Map(document.getElementById("map-canvas"),mapDemo.mapOptions);
        mapDemo.markers = [];
        for (var i = 0; i < data.length; i++) {
                mapDemo.markers.push( new google.maps.Marker({
                position: new google.maps.LatLng(data[i].location.latitude,data[i].location.longitude),
                map:mapDemo.map,
                title:data[i].name,
                id: data[i].id
                }))
                google.maps.event.addListener(mapDemo.markers[i],'click', alert_position);
        } 
        $('#clear').click(clear_map);
        $('#cafe').click(populate);
}

function populate () {
        $.ajax({
        url: 'http://128.208.132.98/html/class/spots.php',
        data: {
                campus: 'seattle'
        },
        type: 'GET',
        success: ajax_success,
        error: ajax_failure
        });

} 

function ajax_failure (error) {
        console.log('error');
}

function alert_position(){
        $('#description').html(data[this.id-1].location.description);
        $('#hours').html(data[this.id-1]['hours-today']);
        $('#pic').attr('src',data[this.id-1].thumbnail);
}